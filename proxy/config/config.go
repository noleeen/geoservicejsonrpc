package config

import (
	"fmt"
	"os"
)

type Config struct {
	DB         DB
	DadataKeys DDKey
	Server     Server
	ServerRPC  ServerRPC
	GeoRPC     GeoRPC
}

type ServerRPC struct {
	Port string
	Type string
}

type GeoRPC struct {
	Host string
	Port string
}

type Server struct {
	Port string
}

type DB struct {
	Driver   string
	Host     string
	Port     string
	Username string
	Password string
	DbName   string
	SslMode  string
}

var defaultDbConfig = DB{
	Driver:   "postgres",
	Host:     "localhost",
	Port:     "5432",
	Username: "postgres",
	Password: "secret",
	DbName:   "users",
}

type DDKey struct {
	ApiKey    string
	SecretKey string
}

func NewConfig() Config {
	return Config{
		DB:         getDbConfig(),
		DadataKeys: getDadataKeys(),
		Server:     getServer(),
		ServerRPC:  getServerRPC(),
		GeoRPC:     getGeoRPC(),
	}
}

func (c *Config) GetDsnDB() string {

	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		c.DB.Host,
		c.DB.Port,
		c.DB.Username,
		c.DB.Password,
		c.DB.DbName,
		c.DB.SslMode,
	)

}

func getServer() Server {
	d := Server{}
	d.Port = os.Getenv("SERVER_PORT")

	return d
}

func getServerRPC() ServerRPC {
	d := ServerRPC{}
	d.Port = os.Getenv("RPC_PORT")
	d.Type = os.Getenv("RPC_PROTOCOL")
	return d
}

func getGeoRPC() GeoRPC {
	d := GeoRPC{}
	d.Port = os.Getenv("GEO_RPC_PORT")
	d.Host = os.Getenv("GEO_RPC_HOST")
	return d
}

func getDadataKeys() DDKey {
	d := DDKey{}
	d.ApiKey = os.Getenv("API_KEY_VALUE")
	d.SecretKey = os.Getenv("SECRET_KEY_VALUE")
	return d
}

func getDbConfig() DB {
	db := DB{}
	db.Driver = getEnv("DB_DRIVER", defaultDbConfig.Driver)
	db.Host = getEnv("DB_HOST", defaultDbConfig.Host)
	db.Port = getEnv("DB_PORT", defaultDbConfig.Port)
	db.Username = getEnv("DB_USER", defaultDbConfig.Username)
	db.Password = getEnv("DB_PASSWORD", defaultDbConfig.Password)
	db.DbName = getEnv("DB_NAME", defaultDbConfig.DbName)
	db.SslMode = getEnv("SSL_MODE", defaultDbConfig.SslMode)
	return db
}

func getEnv(key, fallback string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return fallback
}
