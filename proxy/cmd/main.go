package main

import (
	"geoservicejsonrpc/proxy/config"
	"geoservicejsonrpc/proxy/run/app"
	"os"
)

func main() {
	conf := config.NewConfig()

	app := run.NewApp(conf)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)

}
