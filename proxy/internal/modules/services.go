package modules

import (
	"database/sql"
	"geoservicejsonrpc/proxy/config"
	"geoservicejsonrpc/proxy/internal/entities/authEntity"
	aservice "geoservicejsonrpc/proxy/internal/modules/auth/service"
	gservice "geoservicejsonrpc/proxy/internal/modules/geo/service"
	"github.com/go-chi/jwtauth"
	"github.com/redis/go-redis/v9"
)

type Services struct {
	Geo  gservice.GeoServicer
	Auth aservice.AuthServicer
}

func NewServices(db *sql.DB, cacheRedis *redis.Client, conf *config.Config,
	storage *authEntity.StorageAuth, token *jwtauth.JWTAuth) *Services {
	return &Services{
		Geo:  gservice.NewGeoService(db, cacheRedis, conf),
		Auth: aservice.NewAuthService(storage, token),
	}
}
