package modules

import (
	"geoservicejsonrpc/proxy/internal/infrastructure/responder"
	acontroller "geoservicejsonrpc/proxy/internal/modules/auth/controller"
	gcontroller "geoservicejsonrpc/proxy/internal/modules/geo/controller"
	"geoservicejsonrpc/proxy/internal/modules/geo/service"
)

type Controllers struct {
	Auth acontroller.AuthControllerer
	Geo  gcontroller.GeoControllerer
}

func NewControllers(services *Services, serviceRPC *service.GeoServicRPC, respond responder.Responder) *Controllers {
	return &Controllers{
		Auth: acontroller.NewAuthController(services.Auth, respond),
		Geo:  gcontroller.NewGeoController(serviceRPC, respond),
	}

}
