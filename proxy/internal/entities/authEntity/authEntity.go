package authEntity

type StorageAuth struct {
	UsersMap map[string]User
}

type User struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

//entities for authController

type RegisterLoginRequest struct {
}
