package geo

import (
	"fmt"
	"geoservicejsonrpc/proxy/config"
	"log"
	"net/rpc"
	"net/rpc/jsonrpc"
)

func GetClientRPC(conf config.Config, protocol string) *rpc.Client {
	switch protocol {
	case "rpc":
		client, err := rpc.Dial("tcp", fmt.Sprintf("%s:%s", conf.GeoRPC.Host, conf.GeoRPC.Port))
		if err != nil {
			log.Fatal("error init rpc client ", err)
		}
		log.Println("rpc client connected")
		return client
	case "jsonrpc":
		client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", conf.GeoRPC.Host, conf.GeoRPC.Port))
		if err != nil {
			log.Fatal("error init jsonrpc client ", err)
		}
		log.Println("jsonrpc client connected")
		return client
	default:
		log.Println("invalid rpc protocol")
		return nil
	}
}
