package workers

import (
	"fmt"
	"strings"
)

type node struct {
	Key    int
	Height int
	Left   *node
	Right  *node
}

type AVLTree struct {
	Root *node
}

func newNode(key int) *node {
	return &node{Key: key, Height: 1}
}

func (t *AVLTree) Insert(key int) {
	t.Root = insert(t.Root, key)
}

func (t *AVLTree) ToMermaid() string {
	var sb strings.Builder
	ToMermaid(t.Root, &sb)
	return sb.String()
}

func ToMermaid(node *node, sb *strings.Builder) {
	if node == nil {
		return
	}
	sb.WriteString(fmt.Sprintf("  %d\n", node.Key))
	if node.Left != nil {
		sb.WriteString(fmt.Sprintf("  %d --> %d\n", node.Key, node.Left.Key))
	}
	if node.Right != nil {
		sb.WriteString(fmt.Sprintf("  %d --> %d\n", node.Key, node.Right.Key))
	}
	ToMermaid(node.Left, sb)
	ToMermaid(node.Right, sb)
}

func height(node *node) int {
	if node == nil {
		return 0
	}
	return node.Height
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func updateHight(node *node) {
	if node != nil {
		node.Height = 1 + max(height(node.Left), height(node.Right))
	}
}

func getBalance(node *node) int {
	if node == nil {
		return 0
	}
	return height(node.Left) - height(node.Right)
}

func leftRotate(x *node) *node {
	y := x.Right
	x.Right = y.Left
	y.Left = x

	updateHight(x)
	updateHight(y)

	return y
}

func rightRotate(y *node) *node {
	x := y.Left
	y.Left = x.Right
	x.Right = y

	updateHight(y)
	updateHight(x)

	return x
}

func insert(node *node, key int) *node {
	if node == nil {
		return newNode(key)
	}

	if key < node.Key {
		node.Left = insert(node.Left, key)
	} else if key > node.Key {
		node.Right = insert(node.Right, key)
	} else {
		return node
	}

	updateHight(node)
	balance := getBalance(node)

	if balance > 1 {
		if key < node.Left.Key {
			return rightRotate(node)
		}
	}
	if balance < -1 {
		if key > node.Right.Key {
			return leftRotate(node)
		}
	}
	if balance > 1 {
		if key > node.Left.Key {
			node.Left = leftRotate(node.Left)
			return rightRotate(node)
		}
	}
	if balance < -1 {
		if key < node.Right.Key {
			node.Right = rightRotate(node.Right)
			return leftRotate(node)
		}
	}
	return node

}

func GenerateTree(count int) *AVLTree {
	newTree := AVLTree{}
	for i := 0; i < count; i++ {
		newTree.Insert(i)
	}
	return &newTree
}
